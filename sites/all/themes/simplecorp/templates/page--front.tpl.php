<div id="page-wrapper">
<div id="page">
        
    <header role="header" class="container clearfix">
        <div id="header" class="clearfix">
            <div class="header">            
            <?php if ($page['header']) :?>                
                <?php print render($page['header']); ?>
            <?php endif; ?>
            </div>
        </div>
    </header>

    <div id="contenidoprincipal">    
		<?php if ($show_messages && $messages): print $messages; endif; ?>
		
		<div id="area-content">
    		<?php if ($page['area-content']) :?>                
    		  <?php print render($page['area-content']); ?>
    		<?php endif; ?>
		</div>
   
        <div style="display: none;"><?php print render($page['content']); ?></div>
                
    </div>
       
    <footer>
       <div id="footer" class="container clearfix">
		    <?php if ($page['footer']) :?>
			<?php print render($page['footer']); ?>
		    <?php endif; ?>
            <div class="credits">
                <span>Copyright © <?php echo date("Y"); ?></span>
            </div>
		</div>
    </footer>
	    
</div>
</div>