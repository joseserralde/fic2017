<?php print $doctype; ?>
<html lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>"<?php print $rdf->version . $rdf->namespaces; ?>><head<?php print $rdf->profile; ?>>
    <?php print $head; ?>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="X-Frame-Options" content="SAMEORIGIN">
    
    <title><?php print $head_title; ?></title>
    <?php print $styles; ?>

    <?php global $base_path; global $base_root; ?>
    
    <?php if (theme_get_setting('responsive_respond','simplecorp')): ?>
    <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <?php endif; ?>
    <?php print $scripts; ?>
</head>
<?php
    global $user;
    if(isset($user->roles)){foreach($user->roles as $role){}}
?>
    
<body class="<?php print $classes; print " ".$role." ";?>" <?php print $attributes;?>>
    <?php global $user; print "<div class='uid-oculto' style='display: none;'>".$user->uid."</div>"; ?>

    <div id="skip-link">
      <a href="#main-content" class="element-invisible element-focusable"><?php print t('Skip to main content'); ?></a>
    </div>
    <?php print $page_top; ?>
    <?php print $page; ?>
    <?php print $page_bottom; ?>
    
</body>
</html>
