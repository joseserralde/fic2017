<div id="page-wrapper">
<div id="page">
        
    <header role="header" class="container clearfix">
        <div id="header" class="clearfix">
            <div class="header">            
            <?php if ($page['header']) :?>                
                <?php print render($page['header']); ?>
            <?php endif; ?>
            </div>
        </div>
    </header>

	<div id="content" class="clearfix">
		<div class="contenido-interno">
			
			<?php if ($show_messages && $messages): print $messages; endif; ?>

			<?php //print $breadcrumb ?>

			<?php if ($tabs): ?>
				<div class="tabs"><?php print render($tabs); ?></div>
			<?php endif; ?>
			
			<?php if ($title): ?>
				<h1 class="titlepage"><div class="contenidotitlepage"><?php print $title; ?></div></h1>
			<?php endif; ?>

			<?php print render($page['content']); ?>

		</div>	
	</div>

    <footer>
    	<div id="footer" class="container clearfix">
		    <?php if ($page['footer']) :?>
			<?php print render($page['footer']); ?>
		    <?php endif; ?>

		    <div class="credits">
                <span>Copyright © <?php echo date("Y"); ?></span>
            </div>
		</div>
    </footer>
	
</div>
</div>