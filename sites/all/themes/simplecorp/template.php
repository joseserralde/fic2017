<?php 

/**
 * Implements hook_menu_link().
 */
function simplecorp_menu_link($variables) {
  $element = &$variables['element'];

  // If there is a image uploaded to the menu item, replace the title with the image.
  if (isset($element['#localized_options']['content']['image'])) {
    $image = file_load($element['#localized_options']['content']['image']);
    $image_info = image_get_info($image->uri);
    $image_markup = theme_image(array(
        'path' => $image->uri,
        'width' => $image_info['width'],
        'height' => $image_info['height'],
        'attributes' => array(),
      )
    );
    $element['#localized_options']['html'] = true;
    $element['#title'] = $image_markup.$element['#title'];
  }
  return theme_menu_link($variables);
}



/**
 * Return a themed breadcrumb trail.
 *
 * @param $breadcrumb
 *   An array containing the breadcrumb links.
 * @return
 *   A string containing the breadcrumb output.
 */
function simplecorp_breadcrumb($variables){
	$breadcrumb = $variables['breadcrumb'];
	$breadcrumb_separator=theme_get_setting('breadcrumb_separator','simplecorp');

	if (!empty($breadcrumb)) {
	$breadcrumb[] = drupal_get_title();
	return '<div id="breadcrumb">' . implode(' <span class="breadcrumb-separator">' . $breadcrumb_separator . ' </span>' , $breadcrumb) . '</div>';
	}
}


/**
 * Page alter.
 */
function simplecorp_page_alter($page) {
	if (theme_get_setting('responsive_meta','simplecorp')):
		$mobileoptimized = array(
		'#type' => 'html_tag',
		'#tag' => 'meta',
		'#attributes' => array(
		'name' =>  'MobileOptimized',
		'content' =>  'width'
		)
		);

		$handheldfriendly = array(
		'#type' => 'html_tag',
		'#tag' => 'meta',
		'#attributes' => array(
		'name' =>  'HandheldFriendly',
		'content' =>  'true'
		)
		);

		$viewport = array(
		'#type' => 'html_tag',
		'#tag' => 'meta',
		'#attributes' => array(
		'name' =>  'viewport',
		'content' =>  'width=device-width, initial-scale=1'
		)
		);

		drupal_add_html_head($mobileoptimized, 'MobileOptimized');
		drupal_add_html_head($handheldfriendly, 'HandheldFriendly');
		drupal_add_html_head($viewport, 'viewport');
	endif;
}


/**
 * Preprocess function for node.tpl.php.
 */
function simplecorp_preprocess_node(&$variables) {
	$node = $variables['node'];
	$variables['submitted_day'] = format_date($node->created, 'custom', 'j');
	$variables['submitted_month'] = format_date($node->created, 'custom', 'F');
	$variables['submitted_year'] = format_date($node->created, 'custom', 'Y');
}

/**
 * Implements hook_preprocess_html().
 */
function simplecorp_preprocess_html(&$vars) {
  // Ensure that the $vars['rdf'] variable is an object.
  if (!isset($vars['rdf']) || !is_object($vars['rdf'])) {
    $vars['rdf'] = new StdClass();
  }

  if (module_exists('rdf')) {
    $vars['doctype'] = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"  "http://www.w3.org/TR/html4/loose.dtd">' . "\n";
    $vars['rdf']->version = 'version="HTML+RDFa 1.1"';
    $vars['rdf']->namespaces = $vars['rdf_namespaces'];
    $vars['rdf']->profile = ' profile="' . $vars['grddl_profile'] . '"';
  } else {
    $vars['doctype'] = '<!DOCTYPE html>' . "\n";
    $vars['rdf']->version = '';
    $vars['rdf']->namespaces = '';
    $vars['rdf']->profile = '';
  }
  
 // use the $html5shiv variable in their html.tpl.php
  $element = array(  
    'element' => array(
    '#tag' => 'script',
    '#value' => '',
    '#attributes' => array(
      'src' => '//html5shiv.googlecode.com/svn/trunk/html5.js',
     ),
   ),
 );

 $shimset = theme_get_setting('simplecorp_shim');
 $script = theme('html_tag', $element);
 //If the theme setting for adding the html5shim is checked, set the variable.
 if ($shimset == 1) { $vars['html5shim'] = "\n<!--[if lt IE 9]>\n" . $script . "<![endif]-->\n"; }

}

