<?php

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * @param $form
 *   The form.
 * @param $form_state
 *   The form state.
 */
function simplecorp_form_system_theme_settings_alter(&$form, &$form_state) {
   
    $form['mtt_settings'] = array(
        '#type' => 'fieldset',
        '#title' => t('SimpleCorp Theme Settings'),
        '#collapsible' => FALSE,
        '#collapsed' => FALSE,
    );

    $form['mtt_settings']['tabs'] = array(
        '#type' => 'vertical_tabs',
    );

    $form['mtt_settings']['tabs']['breadcrumb'] = array(
        '#type' => 'fieldset',
        '#title' => t('Breadcrumb'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
    );

    $form['mtt_settings']['tabs']['breadcrumb']['breadcrumb_display'] = array(
        '#type' => 'checkbox',
        '#title' => t('Show breadcrumb'),
        '#description'   => t('Use the checkbox to enable or disable the breadcrumb.'),
        '#default_value' => theme_get_setting('breadcrumb_display','simplecorp'),
    );

    $form['mtt_settings']['tabs']['breadcrumb']['breadcrumb_separator'] = array(
        '#type' => 'textfield',
        '#title' => t('Breadcrumb separator'),
        '#default_value' => theme_get_setting('breadcrumb_separator','simplecorp'),
        '#size'          => 5,
        '#maxlength'     => 10,
    );

    $form['mtt_settings']['tabs']['support']['responsive']['responsive_meta'] = array(
        '#type' => 'checkbox',
        '#title' => t('Add meta tags to support responsive design on mobile devices.'),
        '#default_value' => theme_get_setting('responsive_meta','simplecorp'),
    );

    $form['mtt_settings']['tabs']['support']['responsive']['responsive_respond'] = array(
        '#type' => 'checkbox',
        '#title' => t('Add Respond.js [<em>simplecorp/js/respond.min.js</em>] JavaScript to add basic CSS3 media query support to IE 6-8.'),
        '#default_value' => theme_get_setting('responsive_respond','simplecorp'),
        '#description'   => t('IE 6-8 require a JavaScript polyfill solution to add basic support of CSS3 media queries. Note that you should enable <strong>Aggregate and compress CSS files</strong> through <em>/admin/config/development/performance</em>.'),
    );

}